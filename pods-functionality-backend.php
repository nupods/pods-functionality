<?php

namespace PODS\Functionality;


// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}


class Backend
{
   /**
    * Constructor
    *
    * @return void
    */
    public function __construct()
    {
        // Filters
        add_filter('admin_bar_menu', [$this, 'replace_howdy'], 25);
        add_filter('admin_footer_text', [$this, 'editLeftAdminFooterText'], 1, 2);
        add_filter('update_footer', [$this, 'editRightAdminFooterText'], 11);
        add_filter('the_seo_framework_metabox_priority', function() { return 'low';});

        // WP SEO filters
        add_filter('wpseo_metabox_prio', function() { return 'low';});
        add_filter('wpseo_use_page_analysis', '__return_false');
        add_filter('wpseo_dashboard', '__return_false');

        // Actions
        add_action('set_current_user', [$this, 'removeAdminBar']);
        add_action('admin_enqueue_scripts', [$this, 'addAdminStyles']);
        add_action('login_enqueue_scripts', [$this, 'addAdminStyles']);
        add_action('dashboard_glance_items',  [$this, 'editDashboardGlanceItems']);
        add_action('admin_init',  [$this, 'removeDashboardMetadata']);
        add_action('admin_menu',  [$this, 'acfOptionsMenu'], 12);
        add_action('init', [$this, 'acfOptionsMenuFields'], 20);
    }


    /**
     * Edit "Howdy" text in admin bar
     *
     * @return void
     */
    public function replace_howdy($wp_admin_bar)
    {
      $my_account=$wp_admin_bar->get_node('my-account');
      $newtitle = str_replace( 'Howdy,', 'Account: ', $my_account->title );
      $wp_admin_bar->add_node([
          'id' => 'my-account',
          'title' => $newtitle,
      ]);
    }


    /**
     * Edit left footer text
     *
     * @return string
     */
    public function editLeftAdminFooterText($text)
    {
        $text = '<a href="mailto:provost-digital@neu.edu">Provost Digital Strategy</a>';
        return $text;
    }


    /**
     * Edit right footer text
     *
     * @return string
     */
    public function editRightAdminFooterText($text)
    {
        $text = '<a href="http://www.northeastern.edu">Northeastern University</a>';
        return $text;
    }


    /**
     * Remove the Toolbar, but keep available in the Dashboard
     *
     * @return void
     */
    public function removeAdminBar()
    {
        add_filter('show_admin_bar', '__return_false');
    }


    /**
     * Customize admin CSS to admin and login
     *
     * @return void
     */
    public function addAdminStyles()
    {
        wp_enqueue_style('my-admin-theme', plugin_dir_url( __FILE__ ) . 'assets/styles/wp-admin.css');
    }


    /**
     * Add Custom Post Type to WP-ADMIN At a Glance Widget
     *
     * @return void
     */
    public function editDashboardGlanceItems()
    {
        $args = array(
          'public' => true ,
          '_builtin' => false
        );
        $output = 'object';
        $operator = 'and';
        $post_types = get_post_types( $args , $output , $operator );
        foreach( $post_types as $post_type) {
          $num_posts = wp_count_posts( $post_type->name );
          $num = number_format_i18n( $num_posts->publish );
          $text = _n( $post_type->labels->name, $post_type->labels->name , intval( $num_posts->publish ) );
          if (current_user_can( 'edit_posts' )) {
              $cpt_name = $post_type->name;
          }
          echo '<li class="post-count"><tr><a href="edit.php?post_type='.$cpt_name.'"><td class="first b b-' . $post_type->name . '"></td>' . $num . '&nbsp;<td class="t ' . $post_type->name . '">' . $text . '</td></a></tr></li>';
        }

        $taxonomies = get_taxonomies( $args , $output , $operator );
        foreach ($taxonomies as $taxonomy) {
          $num_terms  = wp_count_terms( $taxonomy->name );
          $num = number_format_i18n( $num_terms );
          $text = _n( $taxonomy->labels->name, $taxonomy->labels->name , intval( $num_terms ));
          if (current_user_can( 'manage_categories' )) {
              $cpt_tax = $taxonomy->name;
          }
          echo '<li class="post-count"><tr><a href="edit-tags.php?taxonomy='.$cpt_tax.'"><td class="first b b-' . $taxonomy->name . '"></td>' . $num . '&nbsp;<td class="t ' . $taxonomy->name . '">' . $text . '</td></a></tr></li>';
        }
    }


    /**
     * Remove unnecessary dashboard meta boxes
     *
     * @return void
     */
    public function removeDashboardMetadata()
    {
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
        remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'side' );
    }


    /**
     * Advanced Custom Fields options menu
     */
    public function acfOptionsMenu()
    {
        if (function_exists('acf_add_options_page') && function_exists('acf_add_options_sub_page')) {
            $options = acf_add_options_page([
                'page_title'  => 'Theme Options',
                'menu_title'  => 'Theme Options',
                'menu_slug'   => 'theme-options',
                'capability'  => 'edit_posts',
                'redirect'    => true
            ]);

            $options_settings = acf_add_options_sub_page([
                'title' => 'Settings',
                'parent' => 'theme-options',
                'capability' => 'edit_posts'
            ]);

            $options_homepage = acf_add_options_sub_page([
                'title' => 'Homepage',
                'parent' => 'theme-options',
                'capability' => 'edit_posts'
            ]);
        }
    }


    /**
     * Advanced Custom Fields options menu fields
    */
    public function acfOptionsMenuFields() {
        if(function_exists('acf_add_local_field_group')) {
            acf_add_local_field_group(array (
                'key' => 'group_580a412d21e44',
                'title' => 'Settings',
                'fields' => array (
                    array (
                        'key' => 'field_580a4132dd2b5',
                        'label' => 'Masthead Logo',
                        'name' => 'image_logo',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'return_format' => 'array',
                        'preview_size' => 'medium',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),
                    array (
                        'key' => 'field_580a414fdd2b6',
                        'label' => 'Mobile Logo',
                        'name' => 'image_logo_mobile',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'return_format' => 'array',
                        'preview_size' => 'medium',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),
                    array (
                        'key' => 'field_582b3fa871a64',
                        'label' => 'Google Analytics tracker',
                        'name' => 'text_google_analytics',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => 'UA-XXXXXXX-XX',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                ),
                'location' => array (
                    array (
                        array (
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => 'acf-options-settings',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => 1,
                'description' => '',
            ));
        }
    }
}

new Backend;
