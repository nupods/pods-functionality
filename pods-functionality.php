<?php
/**
 * Plugin Name: PODS Functionality
 * Description: WP plugin for PODS basic functionality
 */

namespace PODS\Functionality;


// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

require_once(dirname( __FILE__ ) . '/pods-functionality-backend.php');
require_once(dirname( __FILE__ ) . '/pods-functionality-frontend.php');
