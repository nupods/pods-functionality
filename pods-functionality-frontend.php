<?php

namespace PODS\Functionality;


// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}


class Frontend
{
    /**
     * Get footer
     *
     * @return void
     */
    public static function chromeFooter()
    {
        return '
            <footer class="chrome__footer" role="contentinfo">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12 col-lg-3">
                      <a class="logo" alt="Northeastern University" href="http://www.northeastern.edu" aria-label="Northeastern logo"></a>
                    </div>
                    <div class="col-xs-12 col-lg-9">
                      <p>
                        <a target="_blank" href="http://myneu.neu.edu/cp/home/displaylogin">MyNEU</a> •
                        <a target="_blank" href="https://prod-web.neu.edu/webapp6/employeelookup/public/main.action">Find Faculty &amp; Staff</a> •
                        <a target="_blank" href="http://www.northeastern.edu/neuhome/adminlinks/findaz.html">Find A-Z</a> •
                        <a target="_blank" href="http://www.northeastern.edu/emergency/index.html">Emergency Information</a> •
                        <a target="_blank" href="http://www.northeastern.edu/search/index.html">Search</a> •
                        <a target="_blank" href="http://www.northeastern.edu/privacy/index.html">Privacy</a>
                      </p>
                      <p>
                        360 Huntington Ave., Boston, Massachusetts 02115 • 617.373.2000 • TTY 617.373.3768
                        <br> &copy; '. date('Y') .' Northeastern University
                      </p>
                    </div>
                  </div>
                </div>
            </footer>
        ';
    }


    /**
     * Get Tag Manager
     *
     * @return void
     */
    public static function getTagManager($environment = 'development')
    {
        if ($environment === 'production') {
            return '
                <!-- Google Tag Manager - Roll-Up Property -->
                <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WGQLLJ"
                height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
                <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({"gtm.start":
                new Date().getTime(),event:"gtm.js"});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!="dataLayer"?"&l="+l:"";j.async=true;j.src=
                "//www.googletagmanager.com/gtm.js?id="+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,"script","dataLayer","GTM-WGQLLJ");</script>
                <!-- End Google Tag Manager -->
            ';
        }
    }


    /**
     * Get Google Analytics
     *
     * @return void
     */
    public static function getGoogleAnalytics($environment = 'development', $tracker)
    {
        if ($environment === 'production') {
            return "
                <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
                  ga('create', '{$tracker}', 'auto');
                  ga('send', 'pageview');
                </script>
            ";
        } else {
            return "<!-- $environment ENV: GA {$tracker} -->";
        }
    }
}

new Frontend;
